class SquareJob{
    constructor(x, y, long, job) {
        this.x = x;
        this.y = y;
        this.long = long;
        this.job = job;
        this.over = false;
    }
    rollover(px, py) {
        if ((this.x < px) && (this.x + this.long > px) && (this.y < py) && (this.y + this.long > py)) {
            this.over = true;
        }
        else{
            this.over = false
        }
    }

    displayJobInfo() {
        fill(255);
        textStyle(NORMAL);
        const textS = 20;
        textAlign(LEFT);
        textSize(textS);
        text("user : " + this.job.user, this.x - this.long / 2, this.y + this.long + textS);
        text("name : " + this.job.name, this.x - this.long / 2, this.y + this.long + (textS * 2));
        text("state : " + this.job.state, this.x - this.long / 2, this.y + this.long + (textS * 3));
        text("queue : " + this.job.queue_name, this.x - this.long / 2, this.y + this.long + (textS * 4));
        text("Submision : " + new Date(this.job.submission_time).toUTCString(), this.x - this.long / 2, this.y + this.long + (textS * 5));
        
    }

    display(r = 200, g = 200, b = 200) {
        rect(this.long*10, this.y, this.long*2,this.long);
        stroke(0);
        let alpha = 0.5;
        if (this.over) { // can add active or inactive effect
            textStyle(BOLD);
            this.displayJobInfo();
            alpha = 1;
            cursor(HAND);
        }
        textAlign(CENTER);
        textSize(this.long / 2);
        if (this.job.state == "Terminated") {
            fill('rgba(255,0,0,' + alpha + ')');
        }
        else if (this.job.state == "Launching") {
            fill('rgba(0,255,0,' + alpha + ')');
        }
        else if (this.job.state == "Finishing") {
            fill('rgba(200,50,50,' + alpha + ')');
        }
        else if (this.job.state == "Waiting") {
            fill('rgba(0,0,255,' + alpha + ')');
        }
        square(this.x, this.y, this.long);
        fill(0);
        text(this.job.id, this.x + this.long / 2, this.y + this.long / 1.4);
        textStyle(NORMAL);
        
        text("Jobs", this.long*11, 100 +this.long /1.4);
        fill(200);    
    }

    actionOver() {
        this.display();
    }




}
