let Resources;
let Jobs;
let FullStructure = {};
let Bubbles = {};
let Squares = {};


async function asyncLoadJSON(url) {
  return new Promise((resolve, reject) => {
    const result = loadJSON(
      url,
      () => resolve(result),
      () => reject()
    )
  });
}

// chargingjobs and resources from the API 
// and stocking them into global vairable
async function preload() { 
  let jobsURL = 'http://localhost:8001/jobs/?start_time=0&stop_time=500000000000&offset=0&limit=500';
  let resourcesURL = 'http://localhost:8001/resources/?offset=0&limit=25&detailed=false';
  Jobs = await asyncLoadJSON(jobsURL);
  Resources = await asyncLoadJSON(resourcesURL);
}


// trying to now how much nodes are presents:
async function loadStructure() {
  for (const res of Resources) {
    let url = "http://localhost:8001/resources/" + res['id'] + "/jobs?limit=50&offset=0";
    let jobsOfThisResource = await asyncLoadJSON(url);
    let resourceWithJobs = {};
    resourceWithJobs[res['id']] = {};
    let listofCompleteJobs = {};
    for (const completeJob in jobsOfThisResource['items']) {
      listofCompleteJobs[jobsOfThisResource['items'][completeJob].id] = Jobs[jobsOfThisResource['items'][completeJob].id];
    }
    resourceWithJobs[res['id']].jobs = listofCompleteJobs;
    resourceWithJobs[res['id']].state = res.state;
    resourceWithJobs[res['id']].available_upto = res.available_upto;
    if (!(res['network_address'] in FullStructure)) { // in case this node is crossed for the 1st time
      FullStructure[res['network_address']] = { resources : resourceWithJobs };
    }
    else { // we had the resource with his jobs to the existing list
      FullStructure[res['network_address']].resources = resourceWithJobs; /////////////////////// test that it works not 100% sure about it
    }
  }    
}

function createBubble() {
  let xPosition = 0;
  let yPosition = windowHeight / 2;
  for (const node in FullStructure) {
    xPosition += (windowWidth / Object.keys(FullStructure).length) / 1.5;
    let squares = {};
    for (const resource in FullStructure[node].resources) {
      for (let job in FullStructure[node].resources[resource].jobs) {
        if (FullStructure[node].resources[resource].jobs[job] != undefined) {
          squares[job] = Squares[job];
        }
      }
    }
    Bubbles[node] = new Bubble(xPosition, yPosition, windowWidth/8, node, FullStructure[node].resources,squares);
  }
}

function createSquare() {
  let sqrtSize = windowWidth / 40;
  let counter = 1;
  for (const job in Jobs) {
    counter++;
    Squares[job] = new SquareJob(sqrtSize * (counter + 10), 100, sqrtSize, Jobs[job]);
  }
}


async function setup() {
  await preload();
  let jobsList = {};
  for (const job in Jobs['items']) {
    jobsList[Jobs['items'][job].id] = Jobs['items'][job];
  }
  Jobs = jobsList;
  Resources = Resources['items'];

  createCanvas(windowWidth, windowHeight);
  textSize(40);
  await loadStructure();
  console.log("90",FullStructure);
  createSquare();
  createBubble();
  }


function draw() {
  cursor(ARROW);
  background(70);
  for (const bubble in Bubbles) {
    Bubbles[bubble].display();
    Bubbles[bubble].rollover(mouseX, mouseY);
  }
  for (const job in Squares) {
    Squares[job].display();
    Squares[job].rollover(mouseX, mouseY);
  }
}

