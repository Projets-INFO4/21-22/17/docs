import React from 'react';
import { Fragment } from 'react';
import {
    Show, Edit, SimpleForm, RadioButtonGroupInput, Button, useUpdateMany, useRefresh, useNotify, useUnselectAll, BulkDeleteButton,
    SimpleShowLayout, TextField, DateField, List, Datagrid, EditButton, Filter, TextInput
} from 'react-admin';
import { VisibilityOff, Cancel, CheckCircle } from '@material-ui/icons';

// The datagrid : the main resource view
export const resourceDataGrid = (props) => (
    <List filters={<ResourceFilter />} bulkActionButtons={<PostBulkActionButtons />} {...props}>
        <Datagrid rowClick="show" expand={<ResourceShowInline />}>
            <TextField source="id" />
            <TextField source="state" />
            <TextField source="network_address" />
            <EditButton />
        </Datagrid>
    </List>
);

// Enable job filtering by Id, state or network adress
const ResourceFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Id" source="id" />
        <TextInput label="State" source="state" />
        <TextInput label="Network address" source="network_address" />
    </Filter>
);

// A detailed view that can be directly opened form the datagradid, very convenient.
const ResourceShowInline = props => (
    <Show title=" " {...props}>
        <SimpleShowLayout>
            <TextField label="CPU" source="cpu" />
            <TextField label="Core" source="core" />
            <TextField label="Host" source="host" />
            <DateField label="Last job date" source="last_job_date" />
        </SimpleShowLayout>
    </Show>
);

// Bulk action buttons allowing the modification of multiple resources state at once.
const PostBulkActionButtons = props => (
    <Fragment>
        <ChangeStateButton buttonIcon={VisibilityOff} state="Absent" {...props} />
        <ChangeStateButton buttonIcon={Cancel} state="Dead" {...props} />
        <ChangeStateButton buttonIcon={CheckCircle} state="Alive" {...props} />
        <BulkDeleteButton {...props} />
    </Fragment>
);

const ChangeStateButton = (props) => {
    const refresh = useRefresh();
    const notify = useNotify();
    const unselectAll = useUnselectAll();
    const [updateMany, { loading }] = useUpdateMany(
        'resources',
        props.selectedIds,
        { state: props.state },
        {
            onSuccess: () => {
                refresh();
                unselectAll('posts');
            },
            onFailure: error => notify('Error: posts not updated', 'warning'),
        }
    );

    return (
        <Button
            label={'Set ' + props.state}
            disabled={loading}
            onClick={updateMany}
        >
            < props.buttonIcon />
        </Button>
    );
};

// The edit form for a resource
export const resourceEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled label="Id" source="id" />
            <RadioButtonGroupInput source="state" choices={[
                { id: 'Absent', name: 'Absent' },
                { id: 'Dead', name: 'Dead' },
                { id: 'Alive', name: 'Alive' },
            ]} />
        </SimpleForm>
    </Edit>
);
