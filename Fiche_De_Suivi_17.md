**Fiche de Suivi
Groupe 17**

KACHA TOM,MAHAMAN NOURY ABDOURAHAMANE,MEIGNEN HUGO,ZHANG KEMING


**17 Janvier 2022 week 1**
- to understand how the dashboar works
    - start to read the tutorial
    - see the presentation video
- start to install the applications we need

**24 Janvier 2022 week 2**
- continue to install all the applications and environments
- continue the tutorial

**31 Janvier 2022 week 3**
- fix the problems of installation last week
- read the report
- get familiar with react-admin
- read and try to understand the code

**07 Février 2022 week 4**
- how react-admin is used in our project
- compare the difference between oar-docker and oar-docker-compose
- continue to read the code

**14 Février 2022 week 5**
- compare the difference between react-admin v3 and version 4.0.0 alpha 1.0
- get familiar with Monica and find out how to use it

**28 Février 2022 week 6**
- preparation for the speech

**29 Février 2022 week 6**
- trying to understand tools
- ask for some explanation

**7 Mars 2022 week 7**
- trying to understand explanations
- start to change the code to react-admin 4.0.0-alpha.1
- p5js learning

**8 Mars 2022 week 7**
- continue to change the code to react-admin 4.0.0-alpha.1
- Monika and Drawgantt integration via iframe
- p5js learning

**14 Mars 2022 week 8**
- debugging of react-admin 4.0.0-alpha.1
- p5js learning

**15 Mars 2022 week 8**
- debugging of react-admin 4.0.0-alpha.1
- p5js learning

**21 Mars 2022 week 9**
- debugging of react-admin 4.0.0-alpha.1
- Reflexion about Monika implementation

**22 Mars 2022 week 9**
- debugging of react-admin 4.0.0-alpha.1
- Begin to implement the new Monika with p5js

**28 Mars 2022 week 10**
- debugging of react-admin 4.0.0-alpha.1
- Continue Monika implementation

**29 Mars 2022 week 10**
- debugging of react-admin 4.0.0-alpha.1
- Continue Monika implementation
- Link Monika with the new API
- learn Kibana and how to integrate it in our project

