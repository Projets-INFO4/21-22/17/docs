# Start oar-docker-compose

   git clone https://github.com/oar-team/oar-docker-compose

   cd oar-docker-compose/dev
   
   git clone https://github.com/oar-team/oar3
   
   #In oar-docker-compose/dev/.env_oar_provisoning.sh make sure to have the following content:
   
   #SRC is relative to the dev folder
   
   SRC=oar3

   #Cloning this repository

   git clone https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/17/docs
   
   cd docs/dashboard
   
   docker-compose up

# Install js dependencies

   #install yarn
   #on debian
   
   apt intall yarnpkg
   
   #on nix
   
   nix-shell -p yarn
   
   cd oar-dashboard && yarn install # use yarnpkg on debian

# Find the frontend ip

   docker inspect dev_frontend_1
   
   172.23.0.5

   Change the proxy url in file `package.json` for the

   docker address and the port 6668 (e.g 172.23.0.5:6668).

# Start the dashboard. 

   yarn install

   yarn start

   #A browser should open at url localhost:3000

The username/password to submit job is docker/docker. 


# For all other uses after the installation is done.

## Go to your dashboard directory :

   ~/oar-docker-compose/dev/dashboard$ docker-compose up

   After this let it run without touching it.

## In another terminal (with the same path), launch connection with the API :
   (make sure you complete the tuto at this page : https://github.com/oar-team/oar-docker-compose#start-fastapi)

   ~/oar-docker-compose/dev/dashboard$ docker exec --user oar --env OARCONFFILE=/etc/oar/oar.conf dev_frontend_1 uvicorn oar.api.app:app --port 8001 --host 0.0.0.0

   After this let it run without touching it.

### /!\ In case of probleme with the API (cors policy problem in the console): /!\

   You have to install an extension on your browser to allow the application to access data from another site(your API).
   (PS : I used the "Moesif Origin & CORS Changer" extension on Chrome Browser and it worked well.)

## In a third terminal (still the same path), launch the app with yarn :

   ~/oar-docker-compose/dev/dashboard$ yarn start

   After this let it run without touching it, the app will open itself in your browser


